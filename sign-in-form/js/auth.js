'use strict';

const signInForm = document.querySelector('.sign-in-htm');
const signInOutput = signInForm.querySelector('.error-message');
const signInSubmit = signInForm.querySelector('.button');

const signUpForm = document.querySelector('.sign-up-htm');
const signUpOutput = signUpForm.querySelector('.error-message');
const signUpSubmit = signUpForm.querySelector('.button');

function signUpProcess(event) {
  event.preventDefault();
  const signUpFormData = new FormData(signUpForm);
  const newFormData = {};
  for (const [k, v] of signUpFormData) {
    newFormData[k] = v;
  }
  const xhr = new XMLHttpRequest()
  xhr.addEventListener('load', (e) => {
    const answer = JSON.parse(xhr.responseText);
    if (answer.error) {
      signUpOutput.innerText = answer.message;
    }
    if (answer.name) {
      signUpOutput.innerText = `Пользователь ${answer.name} успешно зарегистрирован`;
    }
    console.log(xhr.status, xhr.responseText);
  });
  xhr.open('POST', 'https://neto-api.herokuapp.com/signup');
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.send(JSON.stringify(newFormData));
}

function signInProcess(event) {
  event.preventDefault();
  const signInFormData = new FormData(signInForm);
  const newFormData = {};
  for (const [k, v] of signInFormData) {
    newFormData[k] = v;
  }
  const xhr = new XMLHttpRequest()
  xhr.addEventListener('load', (e) => {
    const answer = JSON.parse(xhr.responseText);
    if (answer.error) {
      signInOutput.innerText = answer.message;
    }
    if (answer.name) {
      signInOutput.innerText = `Пользователь ${answer.name} успешно авторизован`;
    }
    console.log(xhr.status, xhr.responseText);
  });
  xhr.open('POST', 'https://neto-api.herokuapp.com/signin');
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.send(JSON.stringify(newFormData));
}

// add events
signUpSubmit.addEventListener('click', signUpProcess);
signInSubmit.addEventListener('click', signInProcess);