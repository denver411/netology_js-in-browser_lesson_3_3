'use strict';

//global variables




//functions
function defaultColor(event) {
  localStorage.defaultColorType = event.currentTarget.querySelector('input').getAttribute('id');
}

function defaultSize(event) {
  localStorage.defaultSizeType = event.currentTarget.querySelector('input').getAttribute('id');
}

function loadCart() {
  //load cart items

  const cartNode = document.querySelector('#quick-cart');
  cartNode.innerText = '';
  let cartPriceTotal = 0;

  const xhrCart = new XMLHttpRequest();

  xhrCart.addEventListener("load", () => {

    const cart = JSON.parse(xhrCart.responseText);

    for (let cartItems of cart) {
      if (cartItems.productId !== 'null') {
        const cartItemParent = cartNode.appendChild(document.createElement('div'));
        cartItemParent.classList.add('quick-cart-product', 'quick-cart-product-static');
        cartItemParent.id = cartItems.productId;
        cartItemParent.style = "opacity: 1;";

        const cartItemWrapper = cartItemParent.appendChild(document.createElement('div'));
        cartItemWrapper.classList.add('quick-cart-product-wrap');

        const cartItemImage = cartItemWrapper.appendChild(document.createElement('img'));
        cartItemImage.src = cartItems.pic;
        cartItemImage.title = cartItems.title;

        const cartItemPrice = cartItemWrapper.appendChild(document.createElement('span'));
        cartItemPrice.style = 'background-color: #000; opacity: .5'
        cartItemPrice.classList.add('s1');

        const cartItemSpan = cartItemWrapper.appendChild(document.createElement('span'));
        cartItemSpan.classList.add('s2');

        const cartItemQuantity = cartItemParent.appendChild(document.createElement('span'));
        cartItemQuantity.classList.add('count', 'hide', 'fadeUp');
        cartItemQuantity.id = `quick-cart-product-count-${cartItems.productId}`;
        cartItemQuantity.innerText = cartItems.quantity;

        const cartItemRemove = cartItemParent.appendChild(document.createElement('span'));
        cartItemRemove.classList.add('quick-cart-product-remove', 'remove');
        cartItemRemove.dataset.id = cartItems.productId;

        cartPriceTotal += cartItems.price * cartItems.quantity;

        //delete from cart
        const deleteButton = document.querySelector('.remove');
        deleteButton.addEventListener('click', deleteFromCart);
      }
    }

    //cart
    const aParentCart = cartNode.appendChild(document.createElement('a'));
    aParentCart.id = 'quick-cart-pay';
    aParentCart.classList.add('cart-ico');
    if (cartPriceTotal) {
      aParentCart.classList.add('open');
    } else {
      aParentCart.classList.remove('open');
    }
    aParentCart.setAttribute('quickbeam', 'cart-pay')

    const cartWrapper = aParentCart.appendChild(document.createElement('span'));

    const cartTitle = cartWrapper.appendChild(document.createElement('strong'));
    cartTitle.classList.add('quick-cart-text');
    cartTitle.innerText = 'Оформить заказ';
    cartTitle.appendChild(document.createElement('br'));

    const cartPrice = cartWrapper.appendChild(document.createElement('span'));
    cartPrice.id = 'quick-cart-price';
    cartPrice.innerText = `$${cartPriceTotal.toFixed(2)}`;
  });
  xhrCart.open("GET", "https://neto-api.herokuapp.com/cart", true);
  xhrCart.send();




}

function addToCart(event) {
  event.preventDefault();
  const addToCartFormData = new FormData(productForm);
  if (!addToCartFormData.has('size') || !addToCartFormData.has('color')){
    alert('Укажите цвет и размер!');
    return;
  };
  addToCartFormData.append('productId', productForm.dataset.productId);

  const xhr = new XMLHttpRequest()
  xhr.addEventListener('load', () => {
    const answer = xhr.responseText;
    if (answer.error) {
      console.log(answer.message);
      //куда-то надо ошибку выводить
    } else {
      console.log(answer);
      loadCart();
    }

  });
  xhr.open('POST', 'https://neto-api.herokuapp.com/cart');
  xhr.send(addToCartFormData);
}

function deleteFromCart(event) {

  const deleteFromCartData = new FormData();
  deleteFromCartData.append('productId', productForm.dataset.productId);

  const xhr = new XMLHttpRequest();
  xhr.open('POST', 'https://neto-api.herokuapp.com/cart/remove');
  xhr.send(deleteFromCartData);

  xhr.addEventListener('load', () => {
    const answer = xhr.responseText;
    if (answer.error) {
      console.log(answer.message);
      //куда-то надо ошибку выводить
    } else {
    loadCart();
    }

  });

}

//code
//colors snippet
document.addEventListener('DOMContentLoaded', () => {

  const xhrColors = new XMLHttpRequest();
  xhrColors.addEventListener("load", (event) => {
    const colors = JSON.parse(xhrColors.responseText);
    const colorsNode = document.querySelector('#colorSwatch');
    for (let color of colors) {

      const divParentColor = colorsNode.appendChild(document.createElement('div'));
      divParentColor.classList.add('swatch-element', 'color');
      divParentColor.classList.add(color.type);
      color.isAvailable ? divParentColor.classList.add('available') : divParentColor.classList.add('soldout');
      divParentColor.dataset.value = color.type;

      const divChildColor = divParentColor.appendChild(document.createElement('div'));
      divChildColor.classList.add('tooltip');
      divChildColor.innerText = color.title;

      const inputColor = divParentColor.appendChild(document.createElement('input'));
      inputColor.id = color.type;
      if (inputColor.id === localStorage.defaultColorType) {
        inputColor.setAttribute('checked', '');
      }
      inputColor.setAttribute('quickbeam', 'color');
      inputColor.type = 'radio';
      inputColor.name = 'color';
      inputColor.value = color.type;
      color.isAvailable ? inputColor.removeAttribute('disabled') : inputColor.setAttribute('disabled', '');

      const labelColor = divParentColor.appendChild(document.createElement('label'));
      labelColor.setAttribute('for', color.type);

      const spanColor = labelColor.appendChild(document.createElement('span'));
      spanColor.style = `border-color: ${color.type};`;

      const imgColor = labelColor.appendChild(document.createElement('img'));
      imgColor.classList.add('crossed-out');
      imgColor.src = "https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886";

      divParentColor.addEventListener('click', defaultColor);
    }

  });
  xhrColors.open("GET", "https://neto-api.herokuapp.com/cart/colors", true);
  xhrColors.send();

});

//sizes snippet
document.addEventListener('DOMContentLoaded', () => {
  const xhrSizes = new XMLHttpRequest();
  xhrSizes.addEventListener("load", (event) => {
    const sizes = JSON.parse(xhrSizes.responseText);
    const sizesNode = document.querySelector('#sizeSwatch');

    for (let size of sizes) {

      const divParentSize = sizesNode.appendChild(document.createElement('div'));
      divParentSize.classList.add('swatch-element', 'size', 'plain', size.type);
      size.isAvailable ? divParentSize.classList.add('available') : divParentSize.classList.add('soldout');
      divParentSize.dataset.value = size.type;

      const inputSize = divParentSize.appendChild(document.createElement('input'));
      inputSize.id = size.type;
      if (inputSize.id === localStorage.defaultSizeType) {
        inputSize.setAttribute('checked', '');
      }
      inputSize.type = 'radio';
      inputSize.name = 'size';
      inputSize.value = size.type;
      size.isAvailable ? inputSize.removeAttribute('disabled') : inputSize.setAttribute('disabled', '');

      const labelSize = divParentSize.appendChild(document.createElement('label'));
      labelSize.innerText = size.type;
      labelSize.setAttribute('for', size.type);

      const imgSize = labelSize.appendChild(document.createElement('img'));
      imgSize.classList.add('crossed-out');
      imgSize.src = "https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886";

      divParentSize.addEventListener('click', defaultSize);
    }

  });

  xhrSizes.open("GET", "https://neto-api.herokuapp.com/cart/sizes", true);
  xhrSizes.send();

});

//cart
document.addEventListener('DOMContentLoaded', () => {
  loadCart();
});


//add to cart event
const productForm = document.querySelector('#AddToCartForm');
const AddToCartButton = productForm.querySelector('#AddToCart');
AddToCartButton.addEventListener('click', addToCart);