'use strict';

const buttons = document.querySelector('.wrap-btns');
const counter = document.querySelector('#counter');

if (!localStorage.counterData) {
  localStorage.counterData = parseInt('0');
}
counter.innerText = localStorage.counterData;

function changeCounter(event) {
  const buttonIncrement = document.querySelector('#increment');
  const buttonDecrement = document.querySelector('#decrement');
  const buttonReset = document.querySelector('#reset');

  if (event.target === buttonIncrement) {
    localStorage.counterData++;
  }
  if (event.target === buttonDecrement && localStorage.counterData > 0) {
    localStorage.counterData--;
  }
  if (event.target === buttonReset) {
    localStorage.counterData = 0;
  }
  counter.innerText = localStorage.counterData;
}

buttons.addEventListener('click', changeCounter);